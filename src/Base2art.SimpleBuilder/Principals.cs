﻿namespace Base2art.SimpleBuilder
{
    using System;
    using System.Linq;
    using System.Security.Authentication;
    using System.Security.Claims;
    using System.Security.Principal;
    using WebClient;

    public static class Principals
    {
        public static void RequireSignedIn(this IPrincipal principal)
        {
            if (principal == null || !principal.Identity.IsAuthenticated)
            {
                throw new AuthenticationException();
            }
        }

        public static string Token(this ClaimsPrincipal principal)
        {
            var result = principal.Claims.First(x => x.Type == "token").Value;
            return result;
        }

        public static IWebClientRequestBuilder CreateRequest(
            this IWebClient client,
            Uri url,
            ClaimsPrincipal principal)
        {
            return client.CreateRequest(url.ToString())
                         .WithHeader("Authorization", "Bearer " + principal.Token())
                         .WithHeader("Accept", "application/json");

//            request.RequestFormat = DataFormat.Json;
//            request.OnBeforeDeserialization = resp =>
//            {
//                resp.ContentType = "application/json";
//            };
//            request.JsonSerializer = new JsonSerializer();
//            request.AddHeader("Authorization", "Bearer " + principal.Token());
//            request.AddHeader("Accept", "application/json");

//            client.AddHandler("application/json", new JsonDeserializer());
//            var request = new RestRequest("api/v1/builds/{buildId}", Method.GET);

//            request.Prep(principal);
//            request.AddUrlSegment("buildId", buildId.ToString());
        }
    }
}