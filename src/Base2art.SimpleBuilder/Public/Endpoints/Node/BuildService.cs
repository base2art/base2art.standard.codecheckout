﻿namespace Base2art.SimpleBuilder.Public.Endpoints
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.SimpleBuilder.dbo;
    using Base2art.SimpleBuilder.Public.Resources;
    using Threading.Tasks;

    public class BuildService
    {
        private readonly IDataStore store;

        private readonly Guid machineId;

        public BuildService(IDataStoreFactory store, Guid machineId, string dbName)
        {
            this.machineId = machineId;
            this.store = store.Create(dbName);
        }

        public async Task<ProjectBuild> GetBuild(Guid buildId, ClaimsPrincipal principal)
        {
            principal.RequireSignedIn();

            return await this.store.SelectSingle<node_build>()
                             .Where(rs => rs.Field(x => x.id, buildId, (x, y) => x == y))
                             .Execute()
                             .Then()
                             .Return(x => new ProjectBuild
                                          {
                                              BuildId = buildId,
                                              PowerShellScript = x.power_shell_script,
                                              SourceControlType = x.source_control_type,
                                              CompletedAt = x.completed_at,
                                              Output = this.GetFile(x.output),
                                              Error = this.GetFile(x.error),
                                              State = (BuildState) x.state
                                          });
        }

        public async Task<ProjectBuild> Build(ProjectBuild buildData, ClaimsPrincipal principal)
        {
            principal.RequireSignedIn();

            if (buildData.BuildId == Guid.Empty)
            {
                buildData.BuildId = Guid.NewGuid();
            }

            var sourceControlData = buildData.SourceControlData.ObjectToDict();

            await this.store.Insert<node_build>()
                      .Record(rs => rs.Field(s => s.id, buildData.BuildId)
                                      .Field(s => s.source_control_type, buildData.SourceControlType)
                                      .Field(s => s.machine_id, this.machineId)
                                      .Field(s => s.source_control_data, sourceControlData)
                                      .Field(s => s.power_shell_script, buildData.PowerShellScript)
                                      .Field(s => s.state, (int) BuildState.Pending))
                      .Execute();

            return buildData;
        }

        private string GetFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                return File.ReadAllText(fileName);
            }

            return fileName;
        }
    }
}