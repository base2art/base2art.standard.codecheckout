﻿namespace Base2art.SimpleBuilder.Public.Endpoints
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.SimpleBuilder.dbo;
    using Base2art.SimpleBuilder.Public.Resources;
    using Threading.Tasks;

    public class ServerService
    {
        private readonly IDataStore store;

        public ServerService(IDataStoreFactory store, string dbName)
        {
            this.store = store.Create(dbName);
        }

        public Task<BuildServer[]> GetServers(ClaimsPrincipal principal, string[] requiredFeatures = null)
        {
            principal.RequireSignedIn();
            requiredFeatures = requiredFeatures ?? new string[0];
            return this.store.Select<build_server>()
                       .WithNoLock()
                       .OrderBy(x => x.Random())
                       .Execute()
                       .Then()
                       .Select(x => new BuildServer
                                    {
                                        Id = x.id,
                                        Name = x.name,
                                        Url = x.url,
                                        FeaturesSupported = Strings.Split(x.list_of_features)
                                    })
                       .Then()
                       .Where(x => requiredFeatures.Length == 0 || this.Contains(x, requiredFeatures))
                       .Then()
                       .ToArray();
        }

        public async Task<BuildServer> AddServer(ClaimsPrincipal principal, BuildServer server)
        {
            principal.RequireSignedIn();

            if (server.Id == Guid.Empty)
            {
                server.Id = Guid.NewGuid();
            }

            await this.store.Insert<build_server>()
                      .Record(rs => rs.Field(x => x.id, server.Id)
                                      .Field(x => x.name, server.Name)
                                      .Field(x => x.url, server.Url)
                                      .Field(x => x.list_of_features, Strings.Join(server.FeaturesSupported)))
                      .Execute();

            return server;
        }

        private bool Contains(BuildServer x, string[] requiredFeatures)
        {
            var serverFeatures = new HashSet<string>(x.FeaturesSupported ?? new string[0], StringComparer.OrdinalIgnoreCase);

            return requiredFeatures.All(serverFeatures.Contains);
        }
    }
}