﻿namespace Base2art.SimpleBuilder.Public.Endpoints
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.SimpleBuilder.dbo;
    using Base2art.SimpleBuilder.Public.Resources;
    using Threading.Tasks;
    using WebClient;
    using WebClient.NetHttp;
    using WebClient.ResponseHandlers;

    public class ProjectBuildService
    {
        private readonly ServerService servers;

        private readonly IDataStore store;

        public ProjectBuildService(IDataStoreFactory store, string dbName)
        {
            if (store == null)
            {
                throw new ArgumentNullException(nameof(store));
            }

            this.store = store.Create(dbName);
            this.servers = new ServerService(store, dbName);
        }

        public Task<Build[]> GetBuildsByProject(Guid projectId, ClaimsPrincipal principal)
        {
            principal.RequireSignedIn();
            return this.store.Select<build>()
                       .Join<project>((x, y) => x.project_id == y.id)
                       .Join<build_server>((x, z, y) => x.server_id == y.id)
                       .OrderBy1(rs => rs.Field(x => x.when_started, ListSortDirection.Descending))
                       .WithNoLock()
                       .Where2(rs => rs.Field(x => x.id, projectId, (x, y) => x == y))
                       .Execute()
                       .Then()
                       .Select(x => this.MapBuild(x))
                       .Then()
                       .ToArray();
        }

        public async Task<BuildInfo[]> GetRecentBuilds(ClaimsPrincipal principal)
        {
            principal.RequireSignedIn();

            var itemsNotDone = await this.store.Select<build>()
                                         .Join<project>((x, y) => x.project_id == y.id)
                                         .Join<build_server>((x, z, y) => x.server_id == y.id)
                                         .OrderBy1(rs => rs.Field(x => x.when_started, ListSortDirection.Descending))
                                         .WithNoLock()
                                         .Where1(rs => rs.Field(x => x.state, null, (x, y) => x == y))
                                         .Execute();

            var itemsDone = await this.store.Select<build>()
                                      .Join<project>((x, y) => x.project_id == y.id)
                                      .Join<build_server>((x, z, y) => x.server_id == y.id)
                                      .OrderBy1(rs => rs.Field(x => x.when_started, ListSortDirection.Descending))
                                      .WithNoLock()
                                      .Where1(rs => rs.Field(x => x.state, null, (x, y) => x != y))
                                      .Limit(20)
                                      .Execute();

            var items = await Task.WhenAll(itemsNotDone.Select(x => this.GetBuildInfo(x.Item1, x.Item2, x.Item3, principal)));
            return items.Union(itemsDone.Select(x => this.MapBuildInfo(x.Item1, x.Item2))).OrderByDescending(x => x.When).ToArray();
        }

        public async Task<BuildInfo> GetBuild(Guid projectId, Guid buildId, ClaimsPrincipal principal)
        {
            principal.RequireSignedIn();

            var item = await this.store.SelectSingle<build>()
                                 .Join<project>((x, y) => x.project_id == y.id)
                                 .Join<build_server>((x, z, y) => x.server_id == y.id)
                                 .WithNoLock()
                                 .Where1(rs => rs.Field(x => x.id, buildId, (x, y) => x == y))
                                 .Where2(rs => rs.Field(x => x.id, projectId, (x, y) => x == y))
                                 .Execute();

            return await this.GetBuildInfo(item.Item1, item.Item2, item.Item3, principal);
        }

        public async Task<Build> StartBuild(Guid projectId, Build build, ClaimsPrincipal principal)
        {
            principal.RequireSignedIn();

            if (projectId != build.ProjectId)
            {
                throw new ArgumentOutOfRangeException(nameof(projectId), "projectId must match the build projectId");
            }

            var foundProject = await this.store.SelectSingle<project>()
                                         .WithNoLock()
                                         .Where(rs => rs.Field(x => x.id, build.ProjectId, (x, y) => x == y))
                                         .Execute();

            if (foundProject == null)
            {
                return null;
            }

            Guid buildId = Guid.NewGuid();
            build.BuildId = buildId;

            var server = await this.servers.GetServers(principal, Strings.Split(foundProject.list_of_features)).Then().FirstOrDefault();

            if (server == null)
            {
                throw new InvalidOperationException("No server available for processing");
            }

            await this.store.Insert<build>()
                      .Record(rs => rs.Field(x => x.id, buildId)
                                      .Field(x => x.project_id, foundProject.id)
                                      .Field(x => x.server_id, server.Id)
                                      .Field(x => x.when_started, DateTime.UtcNow)
                                      .Field(x => x.user, principal.Identity.Name))
                      .Execute();

            var serverUrl = server.Url;

            var pb = new ProjectBuild
            {
                BuildId = build.BuildId,
                PowerShellScript = foundProject.power_shell_script,
                SourceControlType = foundProject.source_control_type,
                SourceControlData = foundProject.source_control_data
            };
            
            await this.Put(principal, serverUrl, pb);
//            request.Prep(principal);

            return await this.GetBuild(projectId, buildId, principal);
        }

        public Task CancelBuild(Guid projectId, Guid buildId, ClaimsPrincipal principal)
        {
            principal.RequireSignedIn();
            throw new NotImplementedException();
            //            var foundProject = await this.store.SelectSingle<build>()
            //                .WithNoLock()
            //                .Join<build_server>((x, y) => x.server_id == y.id)
            //                .Where1(rs => rs.Field(x => x.id, buildId, (x, y) => x == y))
            //                .Execute();
//
            //            if (foundProject == null)
            //            {
            //                return;
            //            }
//
            //            var client = new RestClient(foundProject.Item2.url);
            //            client.AddHandler("application/json", new JsonDeserializer());
            //            var request = new RestRequest("api/v1/build/{buildId}", Method.DELETE);
            //            request.Prep(principal);
//
            //            request.AddUrlSegment("buildId", buildId.ToString());
//
            //            await client.DeleteTaskAsync<ProjectBuild>(request);
        }

        private BuildInfo MapBuildInfo(build item1, project project)
        {
            return new BuildInfo
                   {
                       ProjectName = project.name,
                       BuildId = item1.id,
                       ProjectId = item1.project_id,
                       When = item1.when_started,
                       User = item1.user,
                       State = (BuildState) item1.state,
                       Error = item1.error,
                       Output = item1.output,
                       WhenCompleted = item1.when_ended
                   };
        }

        private Build MapBuild(Tuple<build, project, build_server> x)
        {
            return new Build
                   {
                       BuildId = x.Item1.id,
                       ProjectId = x.Item1.project_id,
                       When = x.Item1.when_started,
                       User = x.Item1.user
                   };
        }

        private async Task<BuildInfo> GetBuildInfo(build item1, project item2, build_server item3, ClaimsPrincipal principal)
        {
            if (item1.state.HasValue)
            {
                if (!IsWorkingForMoreThan24Hours(item1))
                {
                    return MapBuildInfo(item1, item2);
                }
            }

            var projectBuild = await GetAndStoreResult(item1, item2, item3, principal);

            var projectName = item2.name;
            var projectId = item1.project_id;
            var state = projectBuild.State;

            return new BuildInfo
                   {
                       ProjectName = projectName,
                       BuildId = item1.id,
                       ProjectId = projectId,
                       When = item1.when_started,
                       User = item1.user,
                       State = state,
                       Error = projectBuild.Error,
                       Output = projectBuild.Output
                   };
        }

        private async Task<ProjectBuild> GetAndStoreResult(build build, project project, build_server buildServer, ClaimsPrincipal principal)
        {
            Guid buildId = build.id;

            var serverUrl = buildServer.url;

            ProjectBuild projectBuild = await this.Get(principal, serverUrl, buildId);

            var data = projectBuild ?? new ProjectBuild();

            if (data.State == BuildState.ProcessedSuccessfully || data.State == BuildState.ProcessedWithErrors)
            {
                await this.store.Update<build>()
                          .Set(rs => rs.Field(x => x.state, (int) data.State)
                                       .Field(x => x.output, data.Output)
                                       .Field(x => x.error, data.Error)
                                       .Field(x => x.when_ended, data.CompletedAt))
                          .Where(rs => rs.Field(x => x.id, buildId, (x, y) => x == y))
                          .Execute();
            }
            else if (IsWorkingForMoreThan24Hours(build))
            {
                await this.store.Update<build>()
                          .Set(rs => rs.Field(x => x.state, (int) BuildState.ProcessedWithErrors)
                                       .Field(x => x.output, data.Output)
                                       .Field(x => x.error, data.Error + "\r\n\r\nTIMEOUT!")
                                       .Field(x => x.when_ended, DateTime.UtcNow))
                          .Where(rs => rs.Field(x => x.id, buildId, (x, y) => x == y))
                          .Execute();
            }
            else if (data.State == BuildState.Pending && (DateTime.UtcNow - build.when_started).TotalHours > (24 * 7))
            {
                await this.store.Update<build>()
                          .Set(rs => rs.Field(x => x.state, (int) BuildState.ProcessedWithErrors)
                                       .Field(x => x.output, data.Output)
                                       .Field(x => x.error, data.Error + "\r\n\r\nTIMEOUT!")
                                       .Field(x => x.when_ended, DateTime.UtcNow))
                          .Where(rs => rs.Field(x => x.id, buildId, (x, y) => x == y))
                          .Execute();
            }

            return data;
        }

        private async Task<ProjectBuild> Get(ClaimsPrincipal principal, string serverUrl, Guid buildId)
        {
            using (IWebClient client = new SimpleHttpWebClient())
            {
                var uri = new WebClientUri(serverUrl)
                          .WithResourcePath("api/v1/builds/{buildId}")
                          .WithUrlSegment("buildId", buildId.ToString())
                          .ToUri();

                ProjectBuild pb = null;
                await client.CreateRequest(uri, principal)
                            .WithResponseHandler()
                            .WithStatusHandler(HttpStatusCode.OK, new JsonHandler<ProjectBuild>(x => pb = x))
                            .Run();
                return pb;
            }
        }

        private async Task<ProjectBuild> Put(ClaimsPrincipal principal, string serverUrl, ProjectBuild pb)
        {
            using (IWebClient client = new SimpleHttpWebClient())
            {
                var uri = new WebClientUri(serverUrl)
                          .WithResourcePath("api/v1/builds")
                          .ToUri();

                await client.CreateRequest(uri, principal)
                            .WithMethod(HttpMethod.Put)
                            .WithJsonBody(pb)
                            .WithContentType("application/json")
                            .WithResponseHandler()
                            .Run();

                return pb;
            }
        }

        private bool IsWorkingForMoreThan24Hours(build data)
        {
            return data.state.GetValueOrDefault() == (int) BuildState.Working
                   && (DateTime.UtcNow - data.when_started).TotalHours > 24;
            //&& !data.when_ended.HasValue;
        }
    }
}