﻿
namespace Base2art.SimpleBuilder.Public.Endpoints
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.SimpleBuilder.dbo;
    using Base2art.SimpleBuilder.Public.Resources;
    using Threading.Tasks;

    public class ProjectService
    {
        private readonly IDataStore store;
        
        public ProjectService(IDataStoreFactory store, string dbName)
        {
            this.store = store.Create(dbName);
        }
        
        public Task<Project[]> GetProjects(ClaimsPrincipal principal)
        {
            principal.RequireSignedIn();
            
            return this.store.Select<project>()
                .WithNoLock()
                .Execute()
                .Then()
                .Select(this.Map)
                .Then()
                .ToArray();
        }
        
        public Task<Project> GetProject(ClaimsPrincipal principal, Guid projectId)
        {
            principal.RequireSignedIn();
            
            return this.store.SelectSingle<project>()
                .WithNoLock()
                .Where(rs => rs.Field(x => x.id, projectId, (x, y) => x == y))
                .Execute()
                .Then()
                .Return(this.Map);
        }
        
        
        public Task<Project> AddProject(Project project, ClaimsPrincipal principal)
        {
            principal.RequireSignedIn();
            
            if (project.Id == Guid.Empty)
            {
                project.Id = Guid.NewGuid();
            }
            
            var sourceControlData = Strings.ObjectToDict(project.Data);
            
            return this.store.Insert<project>()
                .Record(rs => rs.Field(x => x.id, project.Id)
                        .Field(x => x.name, project.Name)
                        .Field(x => x.power_shell_script, project.PowerShellScript)
                        .Field(x => x.source_control_type, project.SourceControlType)
                        .Field(x => x.source_control_data, sourceControlData)
                        .Field(x => x.list_of_features, project.RequiredFeatures.Join()))
                .Execute()
                .Then()
                .Return(project);
        }

        private Project Map(project x)
        {
            return new Project {
                Id = x.id,
                Name = x.name,
                PowerShellScript = x.power_shell_script,
                SourceControlType = x.source_control_type,
                RequiredFeatures = Strings.Split(x.list_of_features),
                Data = "redacted" //x.source_control_data
            };
        }
    }
}



//        private async Task<RestClient> GetClient(string server, ClaimsPrincipal principal)
//        {
//            var serverObj = await this.GetServer(server, principal);
//            var client = new RestClient(serverObj.BaseUrl);
//            client.AddHandler("application/json", new JsonDeserializer());
//            return client;
//        }
//        public async Task<Project> GetServer(string server, ClaimsPrincipal principal)
//        {
//            var servers = await this.GetServers(principal);
//            return servers.FirstOrDefault(x => string.Equals(x.Name, server, StringComparison.OrdinalIgnoreCase));
//
//        }

//        public async Task<WebSite[]> GetSites(string server, ClaimsPrincipal principal)
//        {
//            var client = await GetClient(server, principal);
//
//            var request = new RestRequest("api/v1/sites", Method.GET);
//            this.Prep(principal, request);
//
//            return await client.ExecuteTaskAsync<List<WebSite>>(request).Then().Return(x => x.Data.ToArray());
//        }
//
//        public async Task<string[]> GetAvailableVersions(string server, string siteName, ClaimsPrincipal principal)
//        {
//            var client = await GetClient(server, principal);
//
//            var request = new RestRequest("api/v1/sites/{siteName}/available-versions", Method.GET);
//            request.AddParameter("siteName", siteName);
//            this.Prep(principal, request);
//
//            return await client.ExecuteTaskAsync<List<string>>(request).Then().Return(x => x.Data.ToArray());
//        }
//
//        public async Task<DeploymentData[]> GetPastDeployments(string server, string siteName, string environment, ClaimsPrincipal principal)
//        {
//            var client = await GetClient(server, principal);
//            var request = new RestRequest("api/v1/sites/{siteName}/environments/{environment}/deployments", Method.GET);
//            request.AddParameter("siteName", siteName);
//            request.AddParameter("environment", environment);
//            this.Prep(principal, request);
//
//            return await client.ExecuteTaskAsync<List<DeploymentData>>(request).Then().Return(x => x.Data.ToArray());
//        }
//
//
//        public async Task DeploySite(string server, string siteName, string environment, string version, ClaimsPrincipal principal)
//        {
//            var client = await GetClient(server, principal);
//            var request = new RestRequest("api/v1/sites/{siteName}/environments/{environment}/version/{version}", Method.PUT);
//            request.AddJsonBody(new object());
//            request.AddUrlSegment("siteName", siteName);
//            request.AddUrlSegment("environment", environment);
//            request.AddUrlSegment("version", version);
//            this.Prep(principal, request);
//
//            var response = await client.ExecuteTaskAsync(request);
//
//            Console.WriteLine(response.Content);
//        }