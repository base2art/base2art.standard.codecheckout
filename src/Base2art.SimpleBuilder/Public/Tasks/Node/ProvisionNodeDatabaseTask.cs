﻿namespace Base2art.SimpleBuilder.Public.Tasks
{
    using System;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.DataStorage.DataDefinition;
    using Base2art.SimpleBuilder.dbo;

    public class ProvisionNodeDatabaseTask
    {
        private readonly IDataStore store;

        private readonly IDbms dbms;

        public ProvisionNodeDatabaseTask(IDbmsFactory dbmsFactory, IDataStoreFactory store, string dbName)
        {
            this.dbms = dbmsFactory.Create(dbName);
            this.store = store.Create(dbName);
        }

        public async Task ExecuteAsync()
        {
            await this.CreateTable(() => this.dbms.CreateTable<node_build>());
        }

        private async Task CreateTable<T>(Func<ITableCreator<T>> par)
        {
            try
            {
                await par().Execute();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return;
            }
        }
    }
}