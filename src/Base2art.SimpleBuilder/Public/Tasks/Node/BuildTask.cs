﻿namespace Base2art.SimpleBuilder.Public.Tasks
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.SimpleBuilder.CodeCheckout;
    using Base2art.SimpleBuilder.dbo;
    using Base2art.SimpleBuilder.Public.Resources;

    public class BuildTask
    {
        private readonly IDataStore store;

        private readonly Guid machine_id;

        private readonly ICodeCheckoutSystem[] systems;

        private readonly string baseLogDir;

        private readonly string baseWorkingDir;

        public BuildTask(
            IDataStoreFactory store,
            Guid machineId,
            string baseWorkingDir,
            string baseLogDir,
            ICodeCheckoutSystem[] systems,
            string dbName)
        {
            this.baseWorkingDir = baseWorkingDir;
            this.baseLogDir = baseLogDir;
            this.systems = systems;
            this.machine_id = machineId;
            this.store = store.Create(dbName);

            Directory.CreateDirectory(baseWorkingDir);
            Directory.CreateDirectory(baseLogDir);
        }

        public async Task ExecuteAsync()
        {
            Guid g = Guid.NewGuid();

            var id = await this.store.SelectSingle<node_build>()
                               .Fields(rs => rs.Field(x => x.id))
                               .WithNoLock()
                               .Where(rs => rs.Field(x => x.machine_id, machine_id, (x, y) => x == y)
                                              .Field(x => x.state, (int) BuildState.Pending, (x, y) => x == y))
                               .OrderBy(x => x.Random())
                               .Execute();

            if (id == null || id.id == Guid.Empty)
            {
                return;
            }

            await this.store.Update<node_build>()
                      .Set(rs => rs.Field(x => x.claimed_by, g).Field(x => x.state, (int) BuildState.Claimed))
                      .Where(rs => rs.Field(x => x.id, id.id, (x, y) => x == y))
                      .Execute();

            await Task.Delay(TimeSpan.FromMilliseconds(0.5));

            var itemToProcess = await this.store.SelectSingle<node_build>()
                                          .Where(rs => rs.Field(x => x.claimed_by, g, (x, y) => x == y)
                                                         .Field(x => x.state, (int) BuildState.Claimed, (x, y) => x == y))
                                          .Execute();

            if (itemToProcess == null)
            {
                return;
            }

            var checkoutLocation = Path.Combine(this.baseWorkingDir, id.id.ToString("N"));
            var outFile = Path.Combine(this.baseLogDir, id.id.ToString("N") + ".out");
            var errFile = Path.Combine(this.baseLogDir, id.id.ToString("N") + ".err");

            await this.store.Update<node_build>()
                      .Set(rs => rs.Field(x => x.state, (int) BuildState.Working)
                                   .Field(x => x.output, outFile)
                                   .Field(x => x.error, errFile))
                      .Where(rs => rs.Field(x => x.id, itemToProcess.id, (x, y) => x == y))
                      .Execute();

            File.WriteAllText(outFile, "Checking out..." + Environment.NewLine);
            File.WriteAllText(errFile, string.Empty);

            await this.Checkout(
                                itemToProcess.source_control_type,
                                itemToProcess.source_control_data,
                                checkoutLocation);

            await this.RunPowershellScript(
                                           itemToProcess.power_shell_script,
                                           checkoutLocation,
                                           outFile,
                                           errFile);

            await this.store.Update<node_build>()
                      .Set(rs => rs.Field(x => x.state, (int) BuildState.ProcessedSuccessfully)
                                   .Field(x => x.output, outFile)
                                   .Field(x => x.error, errFile)
                                   .Field(x => x.completed_at, DateTime.UtcNow))
                      .Where(rs => rs.Field(x => x.id, itemToProcess.id, (x, y) => x == y))
                      .Execute();
        }

        private Task Checkout(
            string source_control_type,
            System.Collections.Generic.Dictionary<string, object> source_control_data,
            string checkoutLocation)
        {
            var system = this.systems.FirstOrDefault(x => string.Equals(x.Type, source_control_type, StringComparison.OrdinalIgnoreCase));
            if (system == null)
            {
                throw new InvalidOperationException("Source Control system not found");
            }

            return system.Checkout(source_control_data, checkoutLocation);
        }

        private Task RunPowershellScript(
            string power_shell_script,
            string checkoutLocation,
            string outFile,
            string errFile)
        {
            return Task.Factory.StartNew(() =>
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();

                startInfo.FileName = @"powershell.exe";
                startInfo.Arguments = @"-File " + power_shell_script.Trim();

                startInfo.RedirectStandardOutput = true;
                startInfo.RedirectStandardError = true;
                startInfo.UseShellExecute = false;
                startInfo.CreateNoWindow = true;
                startInfo.WorkingDirectory = checkoutLocation;

                Process process = new Process();
                process.StartInfo = startInfo;

                //* Set your output and error (asynchronous) handlers

                process.OutputDataReceived += ((x, y) => { File.AppendAllText(outFile, y.Data + Environment.NewLine); });
                process.ErrorDataReceived += ((x, y) => { File.AppendAllText(errFile, y.Data + Environment.NewLine); });

                process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();
                process.WaitForExit();
            });
        }
    }
}