﻿namespace Base2art.SimpleBuilder.Public.Resources
{
    using System;
    
    public class Project
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
        
        public string PowerShellScript { get; set; }
        
        public string SourceControlType { get; set; }
        
        public object Data { get; set; }
        
        public string[] RequiredFeatures { get; set; }
    }
}