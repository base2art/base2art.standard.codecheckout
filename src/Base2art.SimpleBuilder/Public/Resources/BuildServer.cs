﻿namespace Base2art.SimpleBuilder.Public.Resources
{
    using System;
    
    public class BuildServer
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
        
        public string Url { get; set; }
        
        public string[] FeaturesSupported { get; set; }
    }
}
