﻿namespace Base2art.SimpleBuilder.Public.Resources
{
    using System;
    
    public enum BuildState
    {
        Pending = 0,
        Claimed = 1,
        Working = 2,
        ProcessedSuccessfully = 3,
        ProcessedWithErrors = 4
    }
}


