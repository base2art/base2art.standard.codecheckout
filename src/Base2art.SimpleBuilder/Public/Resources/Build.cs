﻿namespace Base2art.SimpleBuilder.Public.Resources
{
    using System;
    
    public class Build
    {
        public Guid ProjectId { get; set; }
        
        public string ProjectName { get; set; }
        
        public Guid BuildId { get; set; }
        
        public DateTime When { get; set; }
        
        public string User { get; set; }
    }
}
