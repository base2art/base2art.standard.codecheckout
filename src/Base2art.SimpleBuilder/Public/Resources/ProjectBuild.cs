﻿
using System;

namespace Base2art.SimpleBuilder.Public.Resources
{
    public class ProjectBuild
    {
        public Guid BuildId { get; set; }
        
        public string PowerShellScript { get; set; }
        
        public string SourceControlType { get; set; }
        
        public object SourceControlData { get; set; }
        
        public BuildState State { get; set; }
        
        public DateTime? CompletedAt { get; set; }
        
        public string Output { get; set; }
        
        public string Error { get; set; }
    }
}
