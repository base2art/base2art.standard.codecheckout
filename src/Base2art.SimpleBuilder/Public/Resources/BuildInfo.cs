﻿namespace Base2art.SimpleBuilder.Public.Resources
{
    using System;

    public class BuildInfo : Build
    {
        public DateTime? WhenCompleted { get; set; }
        
        public BuildState State { get; set; }
        
        public string Output { get; set; }
        
        public string Error { get; set; }
    }
}


