﻿

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}


function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


function primaryDomain() { return window.location.host.split('.').slice(-2).join("."); }



function signIn() {

    var path = window.location.pathname;
    path = path.substring(1);
    path += "?" + window.location.search;

    var appName = "builder";
    
    if (window.location.host == "localhost" || window.location.host.startsWith("localhost:")) {
        return;
    }
    
    var localSigninServer = window["signinServer"];
    if (localSigninServer == null) {
      localSigninServer = 'https://sso.' + primaryDomain() + '/';
    }
    
    var destLocation = localSigninServer + '?app.name=' + appName + '&returnUrl=' + encodeURIComponent((location.pathname+location.search).substr(1));
    window.location = destLocation;
}


function startup(startupFunction) {

    if (window.location.host == "localhost" || window.location.host.startsWith("localhost:")) {
        startupFunction();
    }


    var auth_jwt = getCookie("auth_jwt");
    var auth_info = getCookie("auth_info");
    if (auth_info && auth_jwt) {
    
        var token = JSON.parse(decodeURIComponent(auth_info));
        var exp = new Date(token.exp * 1000)
        if (exp < new Date()) {
          signIn();
        }
        else {
           startupFunction();
        }
    }
    else
    {
        signIn();
    }
}


$(function(){

$('.include-container').each(function(i, x){
  
  var inc = $(x).attr("data-include");
  $.get(inc).done(function(z){$(x).html(z)});
  
});

});

function mapBuildStatusToHtml(status) {
  
  if (status === "Pending" || status === "0") {
    return '<i class="far fa-clock"></i>';
  }
  else if (status === "Claimed" || status === "1") {
    return "<i class='fas fa-handshake'></i>";
  }
  else if (status === "Working" || status === "2") {
    return "<i class='fas fa-spinner'></i>";
  }
  else if (status === "ProcessedSuccessfully" || status === "3") {
    return "<i class='fas fa-thumbs-up'></i>";
  }
  else if (status === "ProcessedWithErrors" || status === "4") {
    return "<i class='fas fa-thumbs-down'></i>";
  }
  else {
    return "<i class='fas fa-question-circle'></i>";
  }
}

//// FORMS ////


function b2aForms() {
  return {
    
    valueById: function(id) {
      
      return $(document.getElementById(id)).val().trim();
    },
    
    lines: function(value) {
      var ret_lines = value.split(",").map(function(item) { return item.trim(); });
      
      return ret_lines.filter(function(x){return x.length > 0});
    }
  }
}


if (window["$b2a"] == null) {
  window["$b2a"] = b2aForms(); 
}