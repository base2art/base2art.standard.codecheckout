﻿namespace Base2art.SimpleBuilder.dbo
{
    using System;
    using System.Collections.Generic;
    
    public interface node_build
    {
        Guid id { get; }
        
        int state { get; }
        
        Guid machine_id { get; }
        
        Guid? claimed_by { get; }
        
        string source_control_type { get; }
        
        string power_shell_script { get; }
        
        Dictionary<string, object> source_control_data { get; }
        
        string output { get; }
        
        string error { get; }
        
        DateTime? completed_at { get; }
    }
}
