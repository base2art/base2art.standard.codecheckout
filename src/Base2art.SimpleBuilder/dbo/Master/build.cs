﻿namespace Base2art.SimpleBuilder.dbo
{
    using System;
    using System.Collections.Generic;

    public interface build
    {
        Guid id { get; }
        
        Guid project_id { get; }
        
        Guid server_id { get; }
        
        DateTime when_started { get; }
        
        DateTime? when_ended { get; }
        
        int? state { get; }
        
        string output { get; }
        
        string error { get; }
        
        string user { get; }
    }
}


