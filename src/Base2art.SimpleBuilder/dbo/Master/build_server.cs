﻿namespace Base2art.SimpleBuilder.dbo
{
    using System;
    
    public interface build_server
    {
        Guid id { get; }
        
        string name { get; }
        
        string url { get; }
        
        string list_of_features { get; }
    }
}
