﻿namespace Base2art.SimpleBuilder.CodeCheckout
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    public static class ObjectExtensions
    {
        public static void CopyTo<T>(this IDictionary<string, object> source, T destination)
        {
            var someObjectType = typeof(T);
            var properties = someObjectType.GetRuntimeProperties();

            foreach (var item in source)
            {
                var prop = properties.FirstOrDefault(p => string.Equals(p.Name, item.Key, System.StringComparison.OrdinalIgnoreCase));

                prop?.SetValue(destination, item.Value, null);
            }
        }
    }
}