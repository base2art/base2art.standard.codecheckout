﻿namespace Base2art.SimpleBuilder.CodeCheckout
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    
    public abstract class CodeCheckoutSystemBase<TData> : ICodeCheckoutSystem
        where TData : new()
    {
        public abstract string Type { get; }
        
        object ICodeCheckoutSystem.SampleObject => new TData();

        Type ICodeCheckoutSystem.SampleObjectType => typeof(TData);

        Task ICodeCheckoutSystem.Checkout(Dictionary<string, object> parameters, string destinationPath)
        {
            var settings = new TData();
            parameters.CopyTo(settings);
            return this.Checkout(settings, destinationPath);
        }

        protected abstract Task Checkout(TData settings, string destinationPath);
    }
}
