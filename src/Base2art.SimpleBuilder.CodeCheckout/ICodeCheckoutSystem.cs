﻿namespace Base2art.SimpleBuilder.CodeCheckout
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface ICodeCheckoutSystem
    {
        string Type { get; }
        
        object SampleObject { get; }
        
        Type SampleObjectType { get; }
        
        Task Checkout(Dictionary<string, object> parameters, string destinationPath);
    }
}
