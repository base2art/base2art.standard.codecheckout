﻿namespace Base2art.SimpleBuilder.CodeCheckout.Git
{
    public class GitData
    {
        public string CloneLocation { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string KeyName { get; set; }
    }
}
